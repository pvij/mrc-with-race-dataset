# Machine Reading Comprehension with RACE dataset

1. [Task Description](#task-description)
2. [Dataset Description](#dataset-description)

## Task Description
Reading Comprehension has a lot of utilities in the real world e.g., finding the parties' names in a contract, going through a patient's record to find some information etc. Making a machine learn how to ingest text and answer questions based on that text is called Machine Reading Comprehension. Given a passage (or context), a machine should be able to answer questions from it.

## Dataset Description
The dataset used for the task is [RACE: Large-scale ReAding Comprehension Dataset From Examinations](https://arxiv.org/abs/1704.04683). The data has been collected from the English exams for middle and high school Chinese students in the age range between 12 to 18 and consists of nearly 28,000 passages and nearly 1,00,000 questions. Following are the reasons for using this dataset: -
1. There is a huge gap between the baseline performance (43%) and the human performance (95%).
2. Some datasets like CNN/Daily Mail are restricted to few domains which makes it hard to assess the performance of the MRC model over broader topics. RACE dataset solves this problem by covering a variety of topics.
3. RACE dataset is large and can be used for training advanced MRC models.
4. RACE dataset has been exclusively designed by human experts to evaluate the comprehension capabilities of humans. Hence it is only natural to present the same task to the machines to asses their comprehension abilities.
5. Most of the datasets consist of questions whose answers are spans of texts from the given passages (e.g., SQUAD). Although there are some questions in RACE dataset whose answers can directly be extracted from the given passages, it also consists of questions which require reasoning and inference to answer.
6. Datasets like CNN/Daily Mail are automatically generated, bringing a significant amount of noise to the datasets and limiting the performance by domain experts.
 
[RACE dataset](http://www.cs.cmu.edu/~glai1/data/race/) is divided into train, test and dev sets each of which is further split into 'high' (dataset from high school students) and 'middle' (dataset from middle school students) folders. Each of these folders consists of txt files. A sample txt file looks as follows: -

```
{
    "answers": [
        "B",
        "D",
        "A",
        "B"
    ],
    "options": [
        [
            "how they really feel when they are learning",
            "whether mood affects their learning ability",
            "what methods are easy for kids to learn",
            "the relationship between sadness and happiness"
        ],
        [
            "kids who listened to happy music turned out to be energetic",
            "kids who listened to sad music liked to choose smiley faces",
            "kids worked harder in the background of happy music",
            "sad music helped kids find out small shapes quickly"
        ],
        [
            "The researchers will continue to do experiments.",
            "The researchers have found a clear answer.",
            "The experiments are popular among kids.",
            "Kids change their feelings more easily."
        ],
        [
            "a science survey",
            "a research report",
            "a school project",
            "an introduction to an experiment"
        ]
    ],
    "questions": [
        "Researchers did experiments on kids in order to find out   _  .",
        "The researchers found in the first experiment that   _  .",
        "What can we learn from the text?",
        "We can infer that the text is   _  ."
    ],
    "article": "Homework can put you in a badmood , and that might actually be a good thing. Researchers from the University of Plymouth in England doubted whether mood might affect the way kids learn. To find out the answer, they did two experiments with children.\nThe first experiment tested 30 kids. Someshapes  were hidden inside a different, larger picture. The kids had to find the small shapes while sitting in a room with either cheerful or sad music playing in the background. To test their mood, the scientists asked the kids to point to one of five faces, from happy to sad. Children who listened to cheerful music tended to point to the smiley faces while the others pointed to the unhappy ones. The researchers found that sad kids took at least a second less to find the small shapes. They also found an average of three or four more shapes.\nIn the second experiment, 61 children watched one of two scenes from a film. One scene was happy, and the other was sad. Just like in the first experiment, kids who saw the sad scene acted better compared to the others.\nThe researchers guessed that feeling down makes people more likely to focus on a problem or difficult situation. Not all scientists agree with them, however. Other studies argued that maybe, that cheerful music in the first experimentdistracted   kids from finding shapes.\nWhile scientists work on finding out the answers, it still might be wise to choose when to do your tasks according to your mood. After eating a delicious ice cream, for example, write an essay.",
    "id": "high63.txt"
}
```
The above json has the following fields: -<br><br>
`article`: Passage, a string<br>
`questions`: List of questions, each of which is a string. There are two types of questions, interrogative and others which have placeholders represented by _ <br>
`options`: List of lists of options for each question, each nested list is a list of strings; number of options is always 4 for each question<br>
`answers`: List of answers for all questions, each of which is a string<br>
`id`: Each passage has a unique id in this dataset

The values corresponding to the keys `article` , `questions` and `options` would be the input for the problem here. The value for the key `answers` would be the output.

This was an attempt to replicate the results from the paper [RACE: Large-scale ReAding Comprehension Dataset From Examinations](https://arxiv.org/abs/1704.04683).

|Test Set|Paper's Accuracy|My accuracy|
|--------|----------------|-----------|
|RACE-M  |      44.2      |   25.97   |
|RACE-H  |      43.0      |   27.33   |
|RACE    |      43.3      |   26.94   |

