import os
import json
from blingfire import text_to_words
from glob import glob
import numpy as np
import tensorflow as tf
import argparse
from collections import defaultdict


class FeatureWriter(object):
    def __init__(self, filename):
        self.filename = filename
        self.num_features = 0
        self._writer = tf.io.TFRecordWriter(filename)

    def process_feature(self, feature):
        self.num_features += 1

        def create_int_feature(values):
            feature = tf.train.Feature(int64_list=tf.train.Int64List(value=values))
            return feature

        def create_bytes_feature(value):
            """Returns a bytes_list from a string / byte."""
            return tf.train.Feature(bytes_list=tf.train.BytesList(value=value))

        features = dict()
        features["article"] = create_int_feature([feature[0]])
        features["question"] = create_int_feature(feature[1])
        features["option_0"] = create_int_feature(feature[2])
        features["option_1"] = create_int_feature(feature[3])
        features["option_2"] = create_int_feature(feature[4])
        features["option_3"] = create_int_feature(feature[5])
        features["high_or_low"] = create_bytes_feature([feature[6].encode("utf-8")])
        features["answer"] = create_int_feature([feature[7]])
        features["article_len"] = create_int_feature([feature[8]])
        features["question_len"] = create_int_feature([feature[9]])
        features["option_len"] = create_int_feature(feature[10])

        tf_example = tf.train.Example(features=tf.train.Features(feature=features))
        self._writer.write(tf_example.SerializeToString())

    def close(self):
        self._writer.close()


def get_words(article, questions, options):
    words = []
    list_of_questions_tokens = []
    list_of_list_of_options_tokens = []
    article_tokens = text_to_words(article).split()
    words += article_tokens
    for question in questions:
        question_tokens = text_to_words(question).split()
        list_of_questions_tokens.append(question_tokens)
        words += question_tokens
    for list_options in options:
        list_of_options_tokens = []
        for option in list_options:
            option_tokens = text_to_words(option).split()
            words += option_tokens
            list_of_options_tokens.append(option_tokens)
        list_of_list_of_options_tokens.append(list_of_options_tokens)
    # as set in the paper
    vocabulary_size = 50_000
    return (
        article_tokens,
        list_of_questions_tokens,
        list_of_list_of_options_tokens,
        words[:vocabulary_size],
    )


# The following function takes in the dataset_dir_name, the directory should have
# three subdirectories, train, test and dev each of which should further have
# subdirectories high and middle each of which then has txt files containing the
# datapoints
def read_data_and_get_vocab(dataset_dir_name):
    vocab = set()
    articles_lst = []
    datapoints = defaultdict(list)
    for directory in os.listdir(dataset_dir_name):
        # directory is "train", "test" or "dev"
        for subdirectory in os.listdir(os.path.join(dataset_dir_name, directory)):
            # subdirectory is "high" or "middle"
            path_as_str = os.path.join(dataset_dir_name, directory, subdirectory)
            # print(
            #     directory,
            #     " | ",
            #     subdirectory,
            #     " | ",
            #     len([f for f in os.listdir(path_as_str)]),
            # )
            for i, file_name in enumerate(glob(os.path.join(path_as_str, "*.txt"))):
                with open(file_name) as f:
                    f_json = json.load(f)
                answers = f_json["answers"]
                article_tokens, list_of_questions_tokens, list_of_list_of_options_tokens, words = get_words(
                    f_json["article"], f_json["questions"], f_json["options"]
                )
                vocab.update(words)
                articles_lst.append(article_tokens)

                for q_no, (q_tokens, list_of_o_tokens) in enumerate(
                    zip(list_of_questions_tokens, list_of_list_of_options_tokens)
                ):
                    answer = ord(answers[q_no]) % ord("A")
                    o_len = list(map(len, list_of_o_tokens))
                    # the paper mentions that it eliminates any question which does not
                    # have exactly 4 options
                    datapoints[directory].append(
                        [
                            i,
                            q_tokens,
                            list_of_o_tokens[0],
                            list_of_o_tokens[1],
                            list_of_o_tokens[2],
                            list_of_o_tokens[3],
                            subdirectory,
                            answer,
                            len(article_tokens),
                            len(q_tokens),
                            o_len,
                        ]
                    )
    return vocab, articles_lst, datapoints


def get_glove_embeddings(vocab, glove_embeddings_file_path, word_vec_dim):
    vocab_vec_dict = {}
    vocab_idx_dict = {}
    vocab_idx_dict["PAD_VEC"] = 0
    with open(glove_embeddings_file_path, "r") as f:
        for line in f:
            line = line.strip()
            line_lst = line.split()
            is_vector_float = True
            if len(line_lst) == word_vec_dim + 1:
                for i, x in enumerate(line_lst[1:]):
                    try:
                        line_lst[i + 1] = float(x)
                    except ValueError:
                        is_vector_float = False
                        break
                if not is_vector_float:
                    continue
                if line_lst[0] in vocab:
                    vocab_vec_dict[line_lst[0]] = line_lst[1:]
                    vocab_idx_dict[line_lst[0]] = len(vocab_idx_dict)
    return vocab_vec_dict, vocab_idx_dict


def get_embedding_matrix(vocab, glove_embeddings_file_path, word_vec_dim):
    vocab_vec_dict, vocab_idx_dict = get_glove_embeddings(
        vocab, glove_embeddings_file_path, word_vec_dim
    )
    embedding_matrix = np.zeros((len(vocab) + 1, word_vec_dim))
    for w, w_vec in vocab_vec_dict.items():
        embedding_matrix[vocab_idx_dict[w], :] = np.array(w_vec)
    unknown_word_vec = np.mean(embedding_matrix, axis=0)
    vocab_idx_dict["UNK"] = len(vocab) + 1
    embedding_matrix = np.vstack([embedding_matrix, unknown_word_vec])
    return embedding_matrix, vocab_idx_dict


def pad_list_with_zeros(lst, reqd_lst_len):
    return lst + [0] * (reqd_lst_len - len(lst))


def get_article_vs_word_indices_matrix(
    articles_lst, vocab_idx_dict, max_article_tokens_length
):
    article_tokens_dict = {}
    article_tokens_length_lst = []
    for article_id, article_tokens in enumerate(articles_lst):
        article_tokens_dict[article_id] = [
            vocab_idx_dict[t] if t in vocab_idx_dict else vocab_idx_dict["UNK"]
            for t in article_tokens
        ]
        article_tokens_length_lst.append(len(article_tokens_dict[article_id]))
    article_vs_word_indices_matrix = np.zeros(
        (len(articles_lst), max_article_tokens_length)
    )
    for article_id in range(len(articles_lst)):
        article_vs_word_indices_matrix[article_id, :] = np.array(
            pad_list_with_zeros(
                article_tokens_dict[article_id], max_article_tokens_length
            )
        )
    return article_vs_word_indices_matrix


def replace_question_and_options_words_with_indices(
    datapoints, vocab_idx_dict, max_question_len, max_option_len
):
    # As per the paper, each question has exactly 4 options
    n_options = 4
    for datapoint_key, datapoint_val in datapoints.items():
        for i, datapoint in enumerate(datapoint_val):
            question_tokens = datapoint[1].copy()
            datapoints[datapoint_key][i][1] = pad_list_with_zeros(
                [
                    vocab_idx_dict[t] if t in vocab_idx_dict else vocab_idx_dict["UNK"]
                    for t in question_tokens
                ],
                max_question_len,
            )
            for k in range(2, 2 + n_options):
                option_tokens = datapoint[k].copy()
                datapoints[datapoint_key][i][k] = pad_list_with_zeros(
                    [
                        vocab_idx_dict[t]
                        if t in vocab_idx_dict
                        else vocab_idx_dict["UNK"]
                        for t in option_tokens
                    ],
                    max_option_len,
                )
    return datapoints


def write_to_tf_record(datapoints, saving_dir_name):
    for datapoint_key, datapoint_val in datapoints.items():
        tf_record_file_name = datapoint_key + ".tfrecord"
        tf_record_file_path = os.path.join(saving_dir_name, tf_record_file_name)
        writer = FeatureWriter(tf_record_file_path)
        for d in datapoint_val:
            writer.process_feature(d)
        writer.close()


def write_test_datapts_given_tf_rec_file_names(
    test_datapoints, saving_dir_name, tf_record_file_name
):
    tf_record_file_path = os.path.join(saving_dir_name, tf_record_file_name)
    writer = FeatureWriter(tf_record_file_path)
    for d in test_datapoints:
        writer.process_feature(d)
    writer.close()


def write_test_to_tf_record_separately_for_middle_and_high(
    test_datapoints, saving_dir_name
):
    middle_datapoints = [datapt for datapt in test_datapoints if datapt[6] == "middle"]
    high_datapoints = [datapt for datapt in test_datapoints if datapt[6] == "high"]
    write_test_datapts_given_tf_rec_file_names(
        middle_datapoints, saving_dir_name, "test_middle.tfrecord"
    )
    write_test_datapts_given_tf_rec_file_names(
        high_datapoints, saving_dir_name, "test_high.tfrecord"
    )


def get_stats_for_a_q_o_len(datapoints):
    a_lens = [
        datapt_val[8]
        for datapt_vals in datapoints.values()
        for datapt_val in datapt_vals
    ]
    q_lens = [
        datapt_val[9]
        for datapt_vals in datapoints.values()
        for datapt_val in datapt_vals
    ]
    o_lens = [
        o_len
        for datapt_vals in datapoints.values()
        for datapt_val in datapt_vals
        for o_len in datapt_val[10]
    ]
    # need these later for padding article, question and option
    max_a_len = max(a_lens)
    max_q_len = max(q_lens)
    max_o_len = max(o_lens)
    print("***ARTICLE STATS***")
    print("min = ", min(a_lens))
    print("max = ", max_a_len)
    print("mean = ", round(np.mean(a_lens), 2))
    print("sd = ", round(np.std(a_lens), 2))
    print("**QUESTION STATS***")
    print("min = ", min(q_lens))
    print("max = ", max_q_len)
    print("mean = ", round(np.mean(q_lens), 2))
    print("sd = ", round(np.std(q_lens), 2))
    print("***OPTION STATS***")
    print("min = ", min(o_lens))
    print("max = ", max_o_len)
    print("mean = ", round(np.mean(o_lens), 2))
    print("sd = ", round(np.std(o_lens), 2))
    return max_a_len, max_q_len, max_o_len


parser = argparse.ArgumentParser(
    description="Creates embedding matrix, article vs word indices matrix and writes datapoints to tfrecord files"
)
parser.add_argument(
    "dataset_dir_name",
    type=str,
    help="Name of the directory in which the dataset is present. The directory needs to have train, dev and test directories, each of which should have high and middle directories",
)
parser.add_argument(
    "saving_dir_name",
    type=str,
    help="Name of the directory in which embedding matrix, article vs word indices matrix, training data tfrecord, test data tf record, dev tf record",
)
parser.add_argument(
    "glove_embeddings_file_path",
    type=str,
    help="Path to the glove word embeddings file",
)
parser.add_argument("word_vec_dim", type=int, help="Glove word vector dimension")
args = parser.parse_args()
dataset_dir_name = args.dataset_dir_name
saving_dir_name = args.saving_dir_name
word_vec_dim = args.word_vec_dim
glove_embeddings_file_path = args.glove_embeddings_file_path
if (
    os.path.isdir(dataset_dir_name)
    and os.path.isdir(saving_dir_name)
    and os.path.isfile(glove_embeddings_file_path)
):
    vocab, articles_lst, datapoints = read_data_and_get_vocab(dataset_dir_name)
    # a - article, q - question, o - option
    max_a_len, max_q_len, max_o_len = get_stats_for_a_q_o_len(datapoints)
    embedding_matrix, vocab_idx_dict = get_embedding_matrix(
        vocab, glove_embeddings_file_path, word_vec_dim
    )
    np.save(
        os.path.join(saving_dir_name, "word_embedding_matrix.npy"), embedding_matrix
    )
    article_vs_word_indices_matrix = get_article_vs_word_indices_matrix(
        articles_lst, vocab_idx_dict, max_a_len
    )
    # As per the paper, there are 27,933 passages
    # print(article_vs_word_indices_matrix.shape)  # (27933, 1409)
    np.save(
        os.path.join(saving_dir_name, "article_vs_word_indices_matrix.npy"),
        article_vs_word_indices_matrix,
    )
    datapoints = replace_question_and_options_words_with_indices(
        datapoints, vocab_idx_dict, max_q_len, max_o_len
    )
    write_to_tf_record(datapoints, saving_dir_name)
    write_test_to_tf_record_separately_for_middle_and_high(
        datapoints["test"], saving_dir_name
    )
elif not os.path.isdir(dataset_dir_name):
    print("Dataset directory doesn't exist")
elif not os.path.isdir(saving_dir_name):
    print("Saving directory doesn't exist")
elif not os.path.isfile(glove_embeddings_file_path):
    print("Glove word vector embedding file doesn't exist")
