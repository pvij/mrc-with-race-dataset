import tensorflow as tf
import numpy as np
from pathlib import Path
from sklearn.metrics import confusion_matrix

flags = tf.flags
FLAGS = flags.FLAGS

flags.DEFINE_string("model_dir", "./tmp", "Model Directory")
flags.DEFINE_bool("train", True, "train?")
flags.DEFINE_bool("test", True, "test?")
flags.DEFINE_string("dataset_dir", None, "data set dir to dede")
FLAGS.dataset_dir = Path(FLAGS.dataset_dir)


def input_fn_builder(tfrecord_file: Path, is_training, batch_size, repeat=True):
    def input_fn():
        name_to_features = {
            "article": tf.io.FixedLenFeature([], tf.int64),
            "question": tf.io.FixedLenFeature([76], tf.int64),
            "option_0": tf.io.FixedLenFeature([115], tf.int64),
            "option_1": tf.io.FixedLenFeature([115], tf.int64),
            "option_2": tf.io.FixedLenFeature([115], tf.int64),
            "option_3": tf.io.FixedLenFeature([115], tf.int64),
            "high_or_low": tf.io.FixedLenFeature([], tf.string),
            "answer": tf.io.FixedLenFeature([], tf.int64),
            "article_len": tf.io.FixedLenFeature([], tf.int64),
            "question_len": tf.io.FixedLenFeature([], tf.int64),
            "option_len": tf.io.FixedLenFeature([4], tf.int64),
        }

        def _decode_record(record, name_to_features):
            example = tf.io.parse_single_example(record, name_to_features)
            return example

        d = tf.data.TFRecordDataset(tfrecord_file.as_posix())
        drop_remainder = True
        if repeat:
            d = d.repeat()
        if is_training:
            d = d.shuffle(buffer_size=500)
        d = d.map(
            map_func=lambda record: _decode_record(record, name_to_features)
        ).batch(batch_size=batch_size, drop_remainder=drop_remainder)
        return d

    return input_fn


def get_encoding_using_bidirectional_gru(
    text_as_embeddings, num_units, seq_len, text_type, mode, concat_final=False
):
    with tf.variable_scope("bidirectional_gru_" + text_type):
        text_as_emb_with_dropout = tf.layers.dropout(
            inputs=text_as_embeddings,
            # rate=0.2,
            rate=0.5,  # as set in the paper
            noise_shape=[
                text_as_embeddings.get_shape()[0],
                1,
                text_as_embeddings.get_shape()[2],
            ],
            training=mode == tf.estimator.ModeKeys.TRAIN,
        )
        fw = tf.contrib.rnn.GRUBlockCellV2(name="fw", num_units=num_units)
        bw = tf.contrib.rnn.GRUBlockCellV2(name="bw", num_units=num_units)
        init_shape = np.array([text_as_embeddings.get_shape().as_list()[0], num_units])
        outputs, final_output_states = tf.nn.bidirectional_dynamic_rnn(
            fw,
            bw,
            text_as_emb_with_dropout,
            sequence_length=seq_len,
            initial_state_fw=tf.random.normal(shape=init_shape),
            initial_state_bw=tf.random.normal(shape=init_shape),
            dtype=tf.float32,
        )
    if concat_final:
        # end of padded question?
        fw_output, bw_output = final_output_states
        return tf.concat((fw_output, bw_output), axis=1)
    fw_output, bw_output = outputs
    return tf.concat((fw_output, bw_output), axis=2)


def get_softmax_or_logits(X, Y, is_softmax_needed=False):
    prod = tf.layers.dense(
        inputs=X,
        units=Y.get_shape()[1],
        use_bias=False,
        kernel_initializer=tf.random_uniform_initializer(-0.01, 0.01),
        # kernel_initializer=tf.glorot_uniform_initializer,
    )
    prod = tf.matmul(prod, Y)
    if is_softmax_needed:
        return tf.nn.softmax(prod)
    return prod


def get_contexual_embedding_for_option(
    embedding_matrix, option, option_len, hidden_dim, text_type, mode
):
    option = tf.nn.embedding_lookup(embedding_matrix, option)
    o_contexual_emb = get_encoding_using_bidirectional_gru(
        option, hidden_dim, tf.squeeze(option_len), text_type, mode, concat_final=True
    )
    return o_contexual_emb


def model_fn_builder(embedding_matrix, article_vs_word_indices_matrix):
    def model_fn(features, mode):
        # hidden dimensionality set to 128
        hidden_dim = 128
        embedding = tf.get_variable(
            "embedding", shape=embedding_matrix.shape, trainable=False, dtype=tf.float32
        )
        articles = tf.get_variable(
            "articles",
            shape=article_vs_word_indices_matrix.shape,
            trainable=False,
            dtype=tf.int32,
        )

        def init_fn(scaffold, sess):
            sess.run(embedding.initializer, {embedding.initial_value: embedding_matrix})
            sess.run(
                articles.initializer,  # Variable.initializer is an operation
                {
                    articles.initial_value: article_vs_word_indices_matrix
                },  # Variable.initial_value returns the initial value
            )
            tf.logging.info("Embeddings initialized")

        tf.logging.info("--- FEATURES ---")
        for name in sorted(features.keys()):
            tf.logging.info(f"name = {name}, shape = {features[name].shape}")
        question = features["question"]
        question = tf.nn.embedding_lookup(embedding, question)
        q_contextual_emb = get_encoding_using_bidirectional_gru(
            question,
            hidden_dim,
            tf.squeeze(features["question_len"]),
            "question",
            mode,
            concat_final=True,
        )
        article_id = features["article"]
        article = tf.squeeze(tf.to_int32(tf.nn.embedding_lookup(articles, article_id)))
        article = tf.nn.embedding_lookup(embedding, article)
        # a - article, q - question, o - option
        a_contexual_emb = get_encoding_using_bidirectional_gru(
            article, hidden_dim, tf.squeeze(features["article_len"]), "article", mode
        )
        alpha = get_softmax_or_logits(
            a_contexual_emb,
            tf.expand_dims(q_contextual_emb, axis=2),
            is_softmax_needed=True,
        )
        article_relevant_part_wrt_q = tf.matmul(
            tf.transpose(alpha, perm=(0, 2, 1)), a_contexual_emb
        )
        # Converting to shape (batch_size, dim, 1)
        article_relevant_part_wrt_q = tf.transpose(
            article_relevant_part_wrt_q, perm=(0, 2, 1)
        )
        option_len = features["option_len"]
        # 4 options for each question
        n_options = 4
        o_emb_lst = []
        for i in range(n_options):
            o = features[f"option_{i}"]
            o_len = option_len[:, i]
            o_emb_lst.append(
                get_contexual_embedding_for_option(
                    embedding, o, o_len, hidden_dim, f"option_{i}", mode
                )
            )
        # Of shape (batch_size, n_options, dim)
        o_contexual_emb = tf.stack(o_emb_lst, axis=1)
        # Converting to shape (batch_size, n_options)
        logits = tf.squeeze(
            get_softmax_or_logits(o_contexual_emb, article_relevant_part_wrt_q)
        )
        answers = features["answer"]
        is_answer = tf.one_hot(indices=answers, depth=n_options)
        loss = tf.nn.softmax_cross_entropy_with_logits(labels=is_answer, logits=logits)
        tf.summary.histogram("softmax", tf.nn.softmax(logits))
        loss = tf.math.reduce_mean(loss)
        predictions = tf.math.argmax(logits, axis=-1)
        labels = tf.math.argmax(is_answer, axis=-1)
        tp = tf.reduce_mean(tf.to_float(tf.math.equal(predictions, labels)))
        if mode == tf.estimator.ModeKeys.TRAIN:
            scaffold = tf.train.Scaffold(init_fn=init_fn)
            # learning rate and optimizer set as per the paper
            optimizer = tf.train.GradientDescentOptimizer(learning_rate=0.1)
            # optimizer = tf.train.AdamOptimizer(learning_rate=0.001)
            var = tf.trainable_variables()
            grads = tf.gradients(loss, var)
            # gradients with norm more than 10 have to be clipped as per paper,
            # not sure if following is the right way to do it yet
            # clipped_grads = [tf.clip_by_norm(g, clip_norm=10) for g in grads]
            # norm = tf.linalg.global_norm(clipped_grads)
            clipped_grads, norm = tf.clip_by_global_norm(grads, clip_norm=1)
            tf.summary.scalar("grad_norm", norm)
            tf.summary.scalar("tp", tp)
            tf.summary.histogram("predictions", predictions)
            tf.summary.histogram("logits", logits)
            tf.summary.histogram("labels", labels)
            tf.summary.scalar("loss", loss)
            for v in tf.trainable_variables():
                tf.summary.histogram(v.name, v)
            train_op = optimizer.apply_gradients(
                zip(clipped_grads, var),
                global_step=tf.train.get_or_create_global_step(),
            )
            return tf.estimator.EstimatorSpec(
                mode, loss=loss, train_op=train_op, scaffold=scaffold
            )
        if mode == tf.estimator.ModeKeys.EVAL:
            eval_metric_ops = {"tp": tf.metrics.mean(tp)}
            return tf.estimator.EstimatorSpec(
                mode, loss=loss, eval_metric_ops=eval_metric_ops
            )
        if mode == tf.estimator.ModeKeys.PREDICT:
            predictions = {"predictions": predictions, "answers": answers}
            return tf.estimator.EstimatorSpec(mode, predictions=predictions)

    return model_fn


tf.logging.set_verbosity(tf.logging.INFO)
train_tf_record_file = FLAGS.dataset_dir / "train.tfrecord"
dev_tf_record_file = FLAGS.dataset_dir / "dev.tfrecord"
test_middle_tf_rec = FLAGS.dataset_dir / "test_middle.tfrecord"
test_high_tf_rec = FLAGS.dataset_dir / "test_high.tfrecord"
test_tf_rec = FLAGS.dataset_dir / "test.tfrecord"
embedding_matrix_npy_file = FLAGS.dataset_dir / "word_embedding_matrix.npy"
embedding_matrix = np.load(embedding_matrix_npy_file)
article_vs_word_indices_matrix_npy_file = (
    FLAGS.dataset_dir / "article_vs_word_indices_matrix.npy"
)
article_vs_word_indices_matrix = np.load(article_vs_word_indices_matrix_npy_file)
model_fn = model_fn_builder(embedding_matrix, article_vs_word_indices_matrix)
run_config = tf.estimator.RunConfig(
    model_dir=FLAGS.model_dir,
    save_summary_steps=1,
    save_checkpoints_steps=500,
    keep_checkpoint_max=2,
    log_step_count_steps=100,
)
estimator = tf.estimator.Estimator(model_fn=model_fn, config=run_config)
if FLAGS.train:
    eval_spec = tf.estimator.EvalSpec(
        input_fn=input_fn_builder(dev_tf_record_file, is_training=False, batch_size=50),
        steps=10,
        start_delay_secs=0,
        throttle_secs=0,
    )
    train_spec = tf.estimator.TrainSpec(
        input_fn=input_fn_builder(
            train_tf_record_file, is_training=True, batch_size=10
        ),
        max_steps=100_000,
    )
    tf.estimator.train_and_evaluate(estimator, train_spec, eval_spec)


def get_performance_on_test_data(tf_rec_name):
    input_fn = input_fn_builder(
        tfrecord_file=tf_rec_name, is_training=False, batch_size=2, repeat=False
    )
    predictions = []
    answers = []
    count = 0
    tp = 0
    prediction_results = estimator.predict(
        input_fn=input_fn, yield_single_examples=True
    )
    for result in prediction_results:
        predictions.append(result["predictions"])
        answers.append(result["answers"])
        count += 1
        if result["predictions"] == result["answers"]:
            tp += 1
    print(f"tp {tp / count * 100}")
    labels = [0, 1, 2, 3]
    conf_mat = confusion_matrix(answers, predictions, labels=labels)
    # the count of true negatives is C_{0,0} (C_{i,j} is the count of observations
    # known to be in group i but predicted in group j)
    print(conf_mat)


if FLAGS.test:
    print("---------RACE-middle---------")
    get_performance_on_test_data(test_middle_tf_rec)
    print("---------RACE-high-----------")
    get_performance_on_test_data(test_high_tf_rec)
    print("---------RACE----------------")
    get_performance_on_test_data(test_tf_rec)
